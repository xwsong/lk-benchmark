# lk-benchmark

The layer is for collecting benchmark tools for linux kernel.

slab test
--------------------------------------------------------------
1. hackbench
   example:
   hackbench -g 20 -l 10000
   with perf:
   perf stat -e cpu-clock,context-switches,cpu-migrations,page-faults,L1-icache-load-misses ./hackbench -g 20 -l 10000
   link: https://lore.kernel.org/linux-mm/20211011103302.GA65713@kvm.asia-northeast3-a.c.our-ratio-313919.internal/

2. https://lore.kernel.org/bpf/19acbdbb-fc2f-e198-3d31-850ef53f544e@suse.cz/T/#m3508281c29f38ef2852bd0e478fafa3cf2aa3a9b

3. https://gentwo.org/christoph/slub/tests/in-kernel-module/

Other memory test
-------------------------------------------------------------
1. stress-ng
   https://github.com/ColinIanKing/stress-ng

2. netperf
   link: https://github.com/HewlettPackard/netperf

scheduler test
------------------------------------------------------------
1. cycletest
   link1: https://zhuanlan.zhihu.com/p/336381111
   link2: https://lore.kernel.org/lkml/20220925143908.10846-8-vincent.guittot@linaro.org/

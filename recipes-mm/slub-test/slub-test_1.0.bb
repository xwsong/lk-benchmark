SUMMARY = "slub benchmark tool"
DESCRIPTION = "SLUB test"
HOMEPAGE = "https://gentwo.org/christoph/slub/tests/"
AUTHOR = "Christoph Lameter and Mathieu Desnoyers"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://slub_test.c;md5=f86c85108afb11a3e5d7ad5e5814099e"

inherit module

SRC_URI = "file://Makefile \
           file://slub_test.c \
          "

S = "${WORKDIR}"

# The inherit of module.bbclass will automatically name module packages with
# "kernel-module-" prefix as required by the oe-core build environment.

RPROVIDES:${PN} += "kernel-module-slub-test"

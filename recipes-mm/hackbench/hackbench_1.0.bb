SUMMARY = "This is to get amean results for slab performance measurement."
DESCRIPTION = "This is to get amean results for slab performance measurement."

LICENSE = "LGPL-2.1"
#LIC_FILES_CHKSUM = "file://COPYING;md5=4fbd65380cdd255951079008b364516c"

DEPENDS += " perf"

SUMMARY = "Pmbench is a standalone microbenchmark aimed to profile system \
           paging and memory subsystem performance."
DESCRIPTION = "Please refer to the man page (pmbench.1) under /doc for usage."
LICENSE = "CLOSED"

SRCREV = "46a3d394ca7bfc19136b580747dbd26076150ee2"
PV = "0.7+git${SRCREV}"
S = "${WORKDIR}/git"

SRC_URI = "git://bitbucket.org/jisooy/pmbench.git;protocol=https \
           file://0001-Use-related-header-from-bitbake-project.patch \
           file://0001-Build-pmbench-seperately-for-Linux-and-Windows.patch \
"

EXTRA_OEMAKE += "'CC=${CC}' 'RANLIB=${RANLIB}' 'AR=${AR}' 'CFLAGS=${CFLAGS} -I${S}' 'BUILDDIR=${S}'"
TARGET_CC_ARCH += "${LDFLAGS}"

DEPENDS += "libxml2 icu util-linux-libuuid"

do_configure() {
    :
}

do_install() {
    install -m 0755 -d ${D}${bindir}
    install -m 0755 ${S}/pmbench ${D}${bindir}
}
